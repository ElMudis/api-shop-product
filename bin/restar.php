<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$parametros = array(
    "database_host" => "10.8.10.3",
    "database_port" => "null",
    "database_name" => "padrao-qsite",
    "database_user" => "root",
    "database_password" => "vertrigo",
    "mailer_transport" => "gmail",
    "mailer_host" => "smtp.gmail.com",
    "mailer_user" => "homologacao@qualitypress.com.br",
    "mailer_password" => "Fjj&&S6b",
    "mailer_sendTo" => "raul@qualitypress.com.br",
);


$dirPath = __DIR__.'/..';


file_put_contents($dirPath."/app/config/parameters.yml",
'
parameters:
    database_host: '.$parametros['database_host'].'
    database_port: '.$parametros['database_port'].'
    database_name: '.$parametros['database_name'].'
    database_user: '.$parametros['database_user'].'
    database_password: '.$parametros['database_password'].'
    mailer_transport: '.$parametros['mailer_transport'].'
    mailer_host: '.$parametros['mailer_host'].'
    mailer_user: '.$parametros['mailer_user'].'
    mailer_password: '.$parametros['mailer_password'].'
    mailer_sendTo: '.$parametros['mailer_sendTo'].'
    secret: ThisTokenIsNotSoSecretChangeIt

#    session_dir: "%kernel.root_dir%/../var/sessions/%kernel.environment%"
    session_dir: "/var/lib/php/sessions"
     '
);

deleteDirectory($dirPath."/var/cache");
deleteDirectory($dirPath."/var/logs");
deleteDirectory($dirPath."/var/sessions");




deleteDirectory($dirPath."/src/QSite/FrontendBundle/Entity");
mkdir($dirPath."/src/QSite/FrontendBundle/Entity");

deleteDirectory($dirPath."/src/QSite/FrontendBundle/Controller");
mkdir($dirPath."/src/QSite/FrontendBundle/Controller");
mkdir($dirPath."/src/QSite/FrontendBundle/Controller/Formularios");

deleteDirectory($dirPath."/src/QSite/FrontendBundle/Resources/views");
mkdir($dirPath."/src/QSite/FrontendBundle/Resources/views");

deleteDirectory($dirPath."/src/QSite/BackendBundle/Controller/Entidades");
mkdir($dirPath."/src/QSite/BackendBundle/Controller/Entidades");



$mysqli = new mysqli(
    $parametros['database_host'],
    $parametros['database_user'],
    $parametros['database_password'],
    $parametros['database_name']
);

apagarTabelasDoSchema($mysqli);
echo $mysqli->error."\n";

agregandoTabelasBasicas($mysqli,$dirPath);
echo $mysqli->error."\n";

$mysqli->close();

//unlink($dirPath."/")

function apagarTabelasDoSchema($mysqli) {
    $mysqli->query('SET foreign_key_checks = 0');
    if ($result = $mysqli->query("SHOW TABLES"))
    {
        while($row = $result->fetch_array(MYSQLI_NUM))
        {
            $mysqli->query('DROP TABLE IF EXISTS '.$row[0]);
        }
    }

    $mysqli->query('SET foreign_key_checks = 1');
}
function agregandoTabelasBasicas($mysqli,$dirPath) {

    $string = file_get_contents($dirPath."/bin/init.sql");
    $mysqli->multi_query($string);
}


function apagarPasta($dir) {
    if (!file_exists($dir)) {
        return true;
    }

    if (!is_dir($dir)) {
        return unlink($dir);
    }

    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') {
            continue;
        }

        if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
            return false;
        }

    }

    return rmdir($dir);
}

