DROP TABLE IF EXISTS tipo_conteudo;
CREATE TABLE tipo_conteudo (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  slug varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS tipo_banner;
CREATE TABLE tipo_banner (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  slug varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  multiplo tinyint(1) NOT NULL,
  height int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS banner;
CREATE TABLE banner (
  id int(11) NOT NULL AUTO_INCREMENT,
  tipo_banner_id int(11) DEFAULT NULL,
  titulo varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  subtitulo varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  boton varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  link varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  target varchar(255) COLLATE utf8_unicode_ci DEFAULT '_self',
  ordem int(11) NOT NULL,
  tipo varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  registro varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  imagem varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  imagem_name varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  published_at datetime NOT NULL,
  ativo tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  KEY IDX_6F9DB8E78A20777D (tipo_banner_id),
  CONSTRAINT FK_6F9DB8E78A20777D FOREIGN KEY (tipo_banner_id) REFERENCES tipo_banner (id)
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS conteudo;
CREATE TABLE conteudo (
  id int(11) NOT NULL AUTO_INCREMENT,
  parent_id int(11) DEFAULT NULL,
  tipo_conteudo_id int(11) DEFAULT NULL,
  nome varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  tipo varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  titulo varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  texto longtext COLLATE utf8_unicode_ci NOT NULL,
  master tinyint(1) DEFAULT NULL,
  home tinyint(1) NOT NULL,
  multi_idioma tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  KEY IDX_FA4A13F8727ACA70 (parent_id),
  KEY IDX_FA4A13F81F542046 (tipo_conteudo_id),
  CONSTRAINT FK_FA4A13F81F542046 FOREIGN KEY (tipo_conteudo_id) REFERENCES tipo_conteudo (id),
  CONSTRAINT FK_FA4A13F8727ACA70 FOREIGN KEY (parent_id) REFERENCES conteudo (id)
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS empresa;
CREATE TABLE empresa (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome longtext COLLATE utf8_unicode_ci NOT NULL,
  email_contato longtext COLLATE utf8_unicode_ci NOT NULL,
  endereco longtext COLLATE utf8_unicode_ci NOT NULL,
  cidade longtext COLLATE utf8_unicode_ci NOT NULL,
  numero longtext COLLATE utf8_unicode_ci NOT NULL,
  complemento longtext COLLATE utf8_unicode_ci NOT NULL,
  bairro longtext COLLATE utf8_unicode_ci NOT NULL,
  cep longtext COLLATE utf8_unicode_ci NOT NULL,
  estado longtext COLLATE utf8_unicode_ci NOT NULL,
  telefone longtext COLLATE utf8_unicode_ci,
  sac longtext COLLATE utf8_unicode_ci,
  fax longtext COLLATE utf8_unicode_ci,
  celular longtext COLLATE utf8_unicode_ci,
  horarios longtext COLLATE utf8_unicode_ci,
  google_maps longtext COLLATE utf8_unicode_ci,
  google_latitude longtext COLLATE utf8_unicode_ci,
  google_longitude longtext COLLATE utf8_unicode_ci,
  fabrica_endreco longtext COLLATE utf8_unicode_ci NOT NULL,
  fabrica_atendimento longtext COLLATE utf8_unicode_ci NOT NULL,
  fabrica_google_maps longtext COLLATE utf8_unicode_ci NOT NULL,
  emporio_endreco longtext COLLATE utf8_unicode_ci NOT NULL,
  emporio_atendimento longtext COLLATE utf8_unicode_ci NOT NULL,
  emporio_google_maps longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO  empresa VALUES (1,'Nome Empresa','suporte@empresa.com.br','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>Rua Heinrich Hemmer, 2773&nbsp;<br />\r\nBadenfurt - Blumenau- SC&nbsp;<br />\r\nCEP: 89070-272&nbsp;<br />\r\nFone: (47) 3037-5000</p>\r\n','<p>Hor&aacute;rio de funcionamento: Segunda a sexta: 6:55h &agrave;s 17:55h - S&aacute;bados, domingos e feriados: n&atilde;o abrimos.</p>\r\n','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3558.576300650067!2d-49.13908258495568!3d-26.885199483139445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94df1dcc8eb1bbe7%3A0xe96d94ca7cae8ff4!2sR.+Heinrich+Hemmer%2C+2773+-+Badenfurt%2C+Blumenau+-+SC!5e0!3m2!1spt-BR!2sbr!4v1495223907478\" width=\"550\" height=\"250\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>','<p>Rua Alberto Stein, 199 - sala 109 Emp&oacute;rio Vila Germ&acirc;nica - Blumenau- SC&nbsp;</p>\r\n\r\n<p>CEP: 89036-200&nbsp;<br />\r\nFone: (47) 3335-1915</p>\r\n','<p>Hor&aacute;rio de funcionamento: Segunda a s&aacute;bado: 9h &agrave;s 20h - Domingos e feriados: 10h &agrave;s 18h.</p>\r\n','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3558.576300650067!2d-49.13908258495568!3d-26.885199483139445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94df1dcc8eb1bbe7%3A0xe96d94ca7cae8ff4!2sR.+Heinrich+Hemmer%2C+2773+-+Badenfurt%2C+Blumenau+-+SC!5e0!3m2!1spt-BR!2sbr!4v1495223907478\" width=\"550\" height=\"250\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>');



DROP TABLE IF EXISTS frontend_objeto;
CREATE TABLE frontend_objeto (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  nome_lindo varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  nome_lindo_singular varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  nome_lindo_plural varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  web_path varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  controller_path varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  feminino tinyint(1) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS frontend_objeto_parametro;
CREATE TABLE frontend_objeto_parametro (
  id int(11) NOT NULL AUTO_INCREMENT,
  objeto_id int(11) NOT NULL,
  nome varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  nome_lindo varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  tipo varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  objeto_rel varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  parametro_rel varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  assert longtext COLLATE utf8_unicode_ci,
  ordem int(11) NOT NULL,
  valor_padrao varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  valor_generado varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  valor_especial tinyint(1) DEFAULT NULL,
  representativo tinyint(1) NOT NULL,
  multi_idioma tinyint(1) NOT NULL,
  lista tinyint(1) NOT NULL,
  form tinyint(1) NOT NULL,
  filtro tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  KEY IDX_B8FD4BF876F5CD27 (objeto_id),
  CONSTRAINT FK_B8FD4BF876F5CD27 FOREIGN KEY (objeto_id) REFERENCES frontend_objeto (id) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS gerador_pagina;
CREATE TABLE gerador_pagina (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  nome_controller varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  web_path varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  view_path varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  extend_layout tinyint(1) NOT NULL,
  ativo tinyint(1) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS frontend_relacao_objeto_pagina;
CREATE TABLE frontend_relacao_objeto_pagina (
  id int(11) NOT NULL AUTO_INCREMENT,
  pagina_id int(11) NOT NULL,
  tipo varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  objetos_slug varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  listas_objetos varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  conteudos varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  banners varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UNIQ_3386AC7B57991ECF (pagina_id),
  CONSTRAINT FK_3386AC7B57991ECF FOREIGN KEY (pagina_id) REFERENCES gerador_pagina (id) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS idioma;
CREATE TABLE idioma (
  id int(11) NOT NULL AUTO_INCREMENT,
  titulo varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  abreviatura varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  imagem varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  imagem_nome varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  ordem int(11) DEFAULT NULL,
  ativo tinyint(1) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO idioma VALUES (1,'Português(Brasil)','pt-BR','','',0,1);



DROP TABLE IF EXISTS menu_cms;
CREATE TABLE menu_cms (
  id int(11) NOT NULL AUTO_INCREMENT,
  parent_id int(11) DEFAULT NULL,
  nome varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  label varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  uri varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  icone varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  ordem int(11) NOT NULL,
  master tinyint(1) NOT NULL,
  ativo tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  KEY IDX_342E5735727ACA70 (parent_id),
  CONSTRAINT FK_342E5735727ACA70 FOREIGN KEY (parent_id) REFERENCES menu_cms (id) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO  menu_cms VALUES 
(68,NULL,'empresa','Empresa','#','building',-2,0,1),
(13,NULL,'_banners','Banners','banner','image',1,0,1),
(10,NULL,'_conteudos','Conteúdos','conteudo','list-alt',2,0,1),
(69,NULL,'_entidades','Entidades','#','dashboard',3,0,1),
(3,NULL,'usuarios','Usuarios','master/usuario','user',4,0,1),
(11,NULL,'Seo','Seo','seo','html5',5,0,1),
(45,NULL,'Scripts','Scripts','script','code',6,0,1),
(8,68,'dadosEmpresa','Dados Empresa','empresa','building',0,0,1),
(70,NULL,'gerir','Gerador Site','#','gears',0,1,1),
(64,70,'gerirIdiomas','Gerir Idiomas','master/idioma','flag-checkered',0,1,1),
(71,70,'gerirTextoEstatico','Gerir Texto Estático','master/textoestatico','flag-checkered',0,1,1),
(40,70,'gerirBanner','Gerir Banner','master/bannertipo','cogs',1,1,1),
(39,70,'gerirConteudo','Gerir Conteúdo','master/conteudotipo','cogs',2,1,1),
(38,70,'gerirObjetos','Gerir Objetos','master/frontendobjeto','cogs',3,1,1),
(37,70,'gerirPaginas','Gerir Páginas','master/frontendpage','cogs',4,1,1),
(1,NULL,'menucms','Menu::CMS','master/menucms','sitemap',2,1,1)

;





DROP TABLE IF EXISTS script;
CREATE TABLE script (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  codigo longtext COLLATE utf8_unicode_ci NOT NULL,
  header tinyint(1) DEFAULT NULL,
  ativo tinyint(1) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS usuario;
CREATE TABLE usuario (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  username varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  email varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  password varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  ativo tinyint(1) NOT NULL,
  master tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UNIQ_2265B05DE7927C74 (email)
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



INSERT INTO usuario VALUES (1,'qpress','qpress','qpress@gmail.com','4297f44b13955235245b2497399d7a93',1,1);


DROP TABLE IF EXISTS seo;
CREATE TABLE seo (
id INT AUTO_INCREMENT NOT NULL, 
tipo VARCHAR(255) NOT NULL, 
registro VARCHAR(255) NOT NULL, 
titulo VARCHAR(255) DEFAULT NULL, 
palavras_chave VARCHAR(255) DEFAULT NULL, 
descricao VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS traducao;
CREATE TABLE traducao (
	id INT AUTO_INCREMENT NOT NULL, 
	idioma VARCHAR(255) NOT NULL, 
	codigo VARCHAR(255) NOT NULL, 
	texto LONGTEXT NOT NULL, PRIMARY KEY(id)
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS texto_estatico;
CREATE TABLE texto_estatico (
	id INT AUTO_INCREMENT NOT NULL, 
	idioma VARCHAR(255) NOT NULL, 
	codigo VARCHAR(255) NOT NULL, 
    cms tinyint(1) NOT NULL,
	texto LONGTEXT NOT NULL, PRIMARY KEY(id)
) ENGINE=InnoDB AUTO_INCREMENT=2828 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

