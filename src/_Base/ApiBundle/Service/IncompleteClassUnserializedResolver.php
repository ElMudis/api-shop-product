<?php
namespace _Base\ApiBundle\Service;

class IncompleteClassUnserializedResolver
{
	/**
	 * @param $object
	 *
	 * @return array
	 */
	public function fixIncompletedObject($object)
	{
		// preg_replace_callback handler. Needed to calculate new key-length.
		$fix_key = create_function(
			'$matches',
			'return ":" . strlen( $matches[1] ) . ":\"" . $matches[1] . "\"";'
		);
		
		// 1. Serialize the object to a string.
		$dump = serialize( $object );
		
		// 2. Change class-type to 'stdClass'.
		$dump = preg_replace( '/^O:\d+:"[^"]++"/', 'O:8:"stdClass"', $dump );
		
		// 3. Make private and protected properties public.
		$dump = preg_replace_callback( '/:\d+:"\0.*?\0([^"]+)"/', $fix_key, $dump );
		
		// 4. Unserialize the modified object again.
		return json_decode(json_encode(unserialize($dump)), true);
	}
}