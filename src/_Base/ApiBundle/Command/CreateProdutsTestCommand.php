<?php
/**
 * Created by PhpStorm.
 * User: zac
 * Date: 4/12/16
 * Time: 7:46 PM
 */

namespace _Base\ApiBundle\Command;

use _Base\BackendBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateProdutsTestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('api:products:test:create')
            ->setDescription('Creates a test products')
            ->setHelp(
                <<<EOT
                    The <info>%command.name%</info> command creates 5 new products.

<info>php %command.full_name%  name</info>

EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $product1 = new Product();
        $product1->setManager($em);
        $product1->setTitle("Fallout");
        $product1->setPrice(1.99);
        $em->persist($product1);

        $product2 = new Product();
        $product2->setManager($em);
        $product2->setTitle("Don’t Starve");
        $product2->setPrice(2.99);
        $em->persist($product2);

        $product3 = new Product();
        $product3->setManager($em);
        $product3->setTitle("Baldur’s Gate");
        $product3->setPrice(3.99);
        $em->persist($product3);

        $product4 = new Product();
        $product4->setManager($em);
        $product4->setTitle("Icewind Dale");
        $product4->setPrice(4.99);
        $em->persist($product4);

        $product5 = new Product();
        $product5->setManager($em);
        $product5->setTitle("Bloodborne");
        $product5->setPrice(5.99);
        $em->persist($product5);

        $em->flush();

        $output->writeln(
                'Product added with success'
        );

    }
}