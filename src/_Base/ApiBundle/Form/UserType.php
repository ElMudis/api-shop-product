<?php

namespace _Base\ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class)
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => '_Base\ApiBundle\Entity\User',
            'csrf_protection' => false
        ));
    }
}


//curl -X GET -H "Content-Type: application/json" -H "Accept: application/json" http://127.0.0.1:8000/app_dev.php/api/products?access_token=YWVjMDlhZTM2ZGVmM2Q3MDc0YTFhYTI5NzY1NGY2NWEyNjU5ZWNmODdkYjBjZDRkZmJkMjE3ODFhYTMxY2VhZA