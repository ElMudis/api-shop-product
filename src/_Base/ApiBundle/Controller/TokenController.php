<?php

namespace _Base\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TokenController extends \FOS\OAuthServerBundle\Controller\TokenController
{
	/**
	 * Allows you to generate a new access key.
	 * 
	 * ### Exemplo
	 * ```
	 * curl -X GET \
	 *  -H "Accept: application/json" \
	 *  -H "Content-Type: application/json" \
	 *  "/oauth/v2/token?grant_type=password&client_id=__CLIENT_ID__&client_secret=__CLIENT_SECRET__&username=__USERNAME__&password=__PASSWORD__"
	 * ```
	 * 
	 * #### Resposta
	 *
	 *     {
	 *          "access_token": "OGY3NWY3NmY3NDlkNzgwZWNjYjNkMzNmOTMxY2E1ODc2NTEwZjNmZDQ0OTM0NGRiZjI2OTA5MTA5ZDM0YmM2NA",
	 *          "expires_in": 3600,
	 *          "token_type": "bearer",
	 *          "scope": "user",
	 *          "refresh_token": "NWY0MDc1NTEzNWZmMTk2YTU2Y2Y4YWViMTlhZGU4Yjg2NGY4MTc0NTIxMmE2ZjZhYjJkYzJjNjMwZWFmZTU2OQ"
	 *     }
	 *
	 * @ApiDoc(resource=true)
	 * 
	 * @RequestParam(name="client_id", requirements="string")
	 * @RequestParam(name="client_secret", requirements="string")
	 * @RequestParam(name="username", requirements="string")
	 * @RequestParam(name="password", requirements="string")
	 * @RequestParam(name="grant_type", requirements="string")
	 * 
	 * @Post("/oauth/v2/token")
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function tokenAction(Request $request)
	{
		return parent::tokenAction($request);
	}
}