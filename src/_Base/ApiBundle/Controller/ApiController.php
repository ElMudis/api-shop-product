<?php

namespace _Base\ApiBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Util\Codes;
use Hateoas\Configuration\Route;
use Hateoas\Representation\Factory\PagerfantaFactory;
use JMS\Serializer\SerializationContext;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

abstract class ApiController extends FOSRestController implements ClassResourceInterface
{
	private $pagerfantaFactory;
	
	protected static function getDefaultPage()
	{
		return 1;
	}
	
	protected static function getDefaultLimit()
	{
		return 3;
	}
	
	public function __construct()
	{
		$this->pagerfantaFactory = new PagerfantaFactory();
	}
	
	public function getRepository($className)
	{
		return $this->getDoctrine()->getManager()->getRepository($className);
	}
	
	public function createQueryBuilder($repositoryClassName, $alias, $indexBy = null)
	{
		return $this
			->getRepository($repositoryClassName)
			->createQueryBuilder($alias, $indexBy);
	}
	
	public function createPagerFanta($queryBuilder, $page = 1, $limit = 50)
	{
		$pagerfanta = new Pagerfanta(new DoctrineORMAdapter($queryBuilder));
		$pagerfanta->setMaxPerPage($limit);
		$pagerfanta->setCurrentPage($page);
		
		return $pagerfanta;
	}
	
	public function createRepresentation(Pagerfanta $pager, Route $route, $inline = null)
	{
		return $this->pagerfantaFactory->createRepresentation($pager, $route, $inline);
	}
	
	public function createResponseForFindOne($entityClassName, $identifier)
	{
		$object = $this
			->getRepository($entityClassName)
			->findOneById($identifier);
		
		return $this->handleView(
			!is_null($object)
				? $this->view($object, Codes::HTTP_OK)
				: $this->view(null, Codes::HTTP_NOT_FOUND)
		);
	}
	
	public function createReponseForDeleteOne(EntityRepository $repository, $identifier)
	{
		$object = $repository->findOneBy([
			'id' => $identifier
		]);
		
		$code = Codes::HTTP_NOT_FOUND;
		
		if ($object)
		{
			$this->getDoctrine()->getManager()->remove($object);
			$this->getDoctrine()->getManager()->flush();
			
			$code = Codes::HTTP_OK;
		}
		
		return $this->handleView($this->view(null, $code));
	}
	
	public function createResponseForCollection(ParamFetcherInterface $paramFetcher, QueryBuilder $queryBuilder, Route $route = null)
	{
		$page   = $paramFetcher->get('page') ?: self::getDefaultPage();
		$limit  = $paramFetcher->get('limit') ?: self::getDefaultLimit();
		
		$pagerFanta = $this->createPagerFanta($queryBuilder, $page, $limit);
		$paginatedCollection = $this->createRepresentation($pagerFanta, $route);
		
		$view = $this->view($paginatedCollection, Codes::HTTP_OK);
		$view->setSerializationContext(SerializationContext::create()->enableMaxDepthChecks());
		return $this->handleView($view);
	}
}