<?php

namespace _Base\BackendBundle\Util;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator;
use _Base\AdminBundle\Entity\FrontendObjeto;
use _Base\AdminBundle\Entity\TextoEstatico;
use _Base\AdminBundle\Entity\Traducao;
use _Base\AdminBundle\Entity\Util\BaseEntidad;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Yaml\Yaml;
use Twig_Environment as Environment;

class FuncionesEspeciais
{
//    private $entityManager;
//    private $twig;
//    private $container;
//    private $enderecoHome;

    public function __construct(EntityManager $entityManager, Environment $twig, Container $container)
    {
//        $this->entityManager = $entityManager;
//        $this->twig = $twig;
//        $this->container = $container;
//        $this->enderecoHome = $container->get('router')->generate("backend_home");
    }


    private $estadosDoBrasil = null;

    public function getEstadosDoBrasil()
    {
        if ($this->estadosDoBrasil === null) {
            $this->estadosDoBrasil = array(
                'AC' => 'Acre',
                'MA' => 'Maranhão',
                'RJ' => 'Rio de Janeiro',
                'AL' => 'Alagoas',
                'MT' => 'Mato Grosso',
                'RN' => 'Rio Grande do Norte',
                'AP' => 'Amapá',
                'MS' => 'Mato Grosso do Sul',
                'RS' => 'Rio Grande do Sul',
                'AM' => 'Amazonas',
                'MG' => 'Minas Gerais',
                'RO' => 'Rôndonia',
                'BA' => 'Bahia',
                'PA' => 'Pará',
                'RR' => 'Roraima',
                'CE' => 'Ceará',
                'PB' => 'Paraíba',
                'SC' => 'Santa Catarina',
                'DF' => 'Distrito Federal',
                'PR' => 'Paraná',
                'SP' => 'São Paulo',
                'ES' => 'Espírito Santo',
                'PE' => 'Pernambuco',
                'SE' => 'Sergipe',
                'GO' => 'Goiás',
                'PI' => 'Piauí',
                'TO' => 'Tocantins',
            );
        }
        return $this->estadosDoBrasil;
    }

    private $estadosDoBrasilComAmericaLatina = null;

    public function getEstadosDoBrasilComAmericaLatina()
    {
        if ($this->estadosDoBrasilComAmericaLatina === null) {
            $this->estadosDoBrasilComAmericaLatina = array(
                'AML' => 'America Latina',
                'AC' => 'Acre',
                'MA' => 'Maranhão',
                'RJ' => 'Rio de Janeiro',
                'AL' => 'Alagoas',
                'MT' => 'Mato Grosso',
                'RN' => 'Rio Grande do Norte',
                'AP' => 'Amapá',
                'MS' => 'Mato Grosso do Sul',
                'RS' => 'Rio Grande do Sul',
                'AM' => 'Amazonas',
                'MG' => 'Minas Gerais',
                'RO' => 'Rôndonia',
                'BA' => 'Bahia',
                'PA' => 'Pará',
                'RR' => 'Roraima',
                'CE' => 'Ceará',
                'PB' => 'Paraíba',
                'SC' => 'Santa Catarina',
                'DF' => 'Distrito Federal',
                'PR' => 'Paraná',
                'SP' => 'São Paulo',
                'ES' => 'Espírito Santo',
                'PE' => 'Pernambuco',
                'SE' => 'Sergipe',
                'GO' => 'Goiás',
                'PI' => 'Piauí',
                'TO' => 'Tocantins',
            );
        }
        return $this->estadosDoBrasilComAmericaLatina;
    }


}