<?php


namespace _Base\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Hateoas\Configuration\Annotation as Hateoas;

use _Base\BackendBundle\Entity\Base\BaseItemCart;




/**
 *
 *
 * @ORM\Entity(repositoryClass="_Base\BackendBundle\Repository\ItemCartRepository")
 * @ORM\Table(name="back_item_cart")
 */
class ItemCart extends BaseItemCart
{





}
