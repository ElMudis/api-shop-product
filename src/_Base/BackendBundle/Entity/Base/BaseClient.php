<?php

namespace _Base\BackendBundle\Entity\Base;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Hateoas\Configuration\Annotation as Hateoas;


use _Base\BackendBundle\Entity\Cart;


/**
 *
 *
 * @ORM\MappedSuperclass
 */
class BaseClient extends BaseEntity
{
    /**
     *
     *
     * @Serializer\Exclude()
     */
    protected $SERVER_PATH_TO_IMAGE_FOLDER = "uploads/client";

    /**
     *
     *
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * Name
     *
     * @var string
     *
     * @ORM\Column(type="string",  nullable = false )
     * @Assert\NotBlank(message = "Name e obrigatorio")
     */
    protected $name;
    /**
     * E-mail
     *
     * @var string
     *
     * @ORM\Column(type="string",  nullable = false )
     * @Assert\NotBlank(message = "E-mail e obrigatorio")
     */
    protected $email;
    /**
     * One Client have one Cart.
     *
     * @ORM\OneToOne(targetEntity="Cart", inversedBy="client")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $cart;


    public static function getCamposObrigatorios()
    {
        return array('name', 'email');
    }

    public static function getCampos()
    {
        return array('name', 'email', 'cart');
    }

    public static function getFrontendObjetoNome()
    {
        return "client";
    }

    public function getCamposTraducoes()
    {
        return array(
            'name',
            'email',
        );
    }

    /**
     *
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }


    /**
     * Constructor
     */
    public function __construct()
    {
    }


    /**
     * Set cart
     *
     * @param \_Base\BackendBundle\Entity\Cart $cart
     *
     * @return Client
     */
    public function setCart($cart)
    {
        if (!is_object($cart) && !is_null($cart)) {
            $em = $this->em;
            $cart = $em->getRepository('BackendBundle:Cart')->find($cart);
        }
        $this->cart = $cart;
        return $this;
    }

    /**
     * Get cart
     *
     * @return \_Base\BackendBundle\Entity\Cart
     */
    public function getCart()
    {
        return $this->cart;
    }


    public static function getListaCart(\Symfony\Component\DependencyInjection\Container $container)
    {
        $em = $container->get('doctrine')->getManager();

        $dql = "SELECT a.id as id, a1.texto as titulo FROM BackendBundle:Cart a LEFT JOIN BackendBundle:Traducao a1 WITH a.totalPrice = a1.codigo";
        $equiposTestes = $em->createQuery($dql)->execute();

        $listaEquipeTest = array();
        foreach ($equiposTestes as $equipo) {
            $listaEquipeTest[$equipo['id']] = $equipo['titulo'];
        }

        return $listaEquipeTest;

    }


}
