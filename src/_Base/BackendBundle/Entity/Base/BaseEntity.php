<?php

namespace _Base\BackendBundle\Entity\Base;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectManagerAware;
use Symfony\Component\Debug\Exception\UndefinedMethodException;
use JMS\Serializer\Annotation as Serializer;

class BaseEntity implements ObjectManagerAware
{
    /**
     * @var  ObjectManager $em
     *
     * @Serializer\Exclude()
     */
    protected $em;

    /**
     * @Serializer\Exclude()
     */
    private $file;

    /**
     * Set opcoes
     *
     * @param integer $opcoes
     *
     * @return BannerTipo
     */
    public function setParametroJSON($opcoes)  //Tem que terminar
    {
        $opcoesValores = $opcoes;
        $opcoesArray = json_decode($this->opcoes, true);

        foreach ($opcoesValores as $key => $opcao) {
            if ($key != "__delete__") {
                $campos = explode("-", $key);
                if (count($campos) == 1) {
                    $opcoesArray[$campos[0]]['valor'] = $opcoesValores[$key];
                } elseif (count($campos) == 2) {
                    $opcoesArray[$campos[0]][$campos[1]]['valor'] = $opcoesValores[$key];
                } elseif (count($campos) == 3) {
                    $opcoesArray[$campos[0]][$campos[1]][$campos[2]]['valor'] = $opcoesValores[$key];
                } elseif (count($campos) == 4) {
                    $opcoesArray[$campos[0]][$campos[1]][$campos[2]][$campos[3]]['valor'] = $opcoesValores[$key];
                } elseif (count($campos) == 5) {
                    $opcoesArray[$campos[0]][$campos[1]][$campos[2]][$campos[3]][$campos[4]]['valor'] = $opcoesValores[$key];
                } elseif (count($campos) == 6) {
                    $opcoesArray[$campos[0]][$campos[1]][$campos[2]][$campos[3]][$campos[4]][$campos[5]]['valor'] = $opcoesValores[$key];
                }
            }
        }
        $camposDelete = explode("***", $opcoes['__delete__']);
        foreach ($camposDelete as $key => $opcao) {
            $campos = explode("-", $opcao);
            if (count($campos) == 1) {
                unset($opcoesArray[$campos[0]]);
            } elseif (count($campos) == 2) {
                unset($opcoesArray[$campos[0]][$campos[1]]);
            } elseif (count($campos) == 3) {
                unset($opcoesArray[$campos[0]][$campos[1]][$campos[2]]);
            } elseif (count($campos) == 4) {
                unset($opcoesArray[$campos[0]][$campos[1]][$campos[2]][$campos[3]]);
            } elseif (count($campos) == 5) {
                unset($opcoesArray[$campos[0]][$campos[1]][$campos[2]][$campos[3]][$campos[4]]);
            } elseif (count($campos) == 6) {
                unset($opcoesArray[$campos[0]][$campos[1]][$campos[2]][$campos[3]][$campos[4]]);
            }
        }

        foreach ($opcoesArray as $key => $arrayObject) {
            if (is_array($arrayObject)
                && isset($arrayObject['tipo'])
                && $arrayObject['tipo'] == 'array'
                && isset($arrayObject['valor'])
                && is_array($arrayObject['valor'])
            ) {
                $opcoesArray[$key]['valor'] = array_values($arrayObject['valor']);
            }

        }


        $this->opcoes = json_encode($opcoesArray);

        return $this;
    }


    /**
     * Injects responsible ObjectManager and the ClassMetadata into this persistent object.
     *
     * @param ObjectManager $objectManager
     * @param ClassMetadata $classMetadata
     *
     * @return void
     */
    public function injectObjectManager(ObjectManager $objectManager, ClassMetadata $classMetadata)
    {
        $this->em = $objectManager;
    }

    public function setManager(ObjectManager $objectManager)
    {
        $this->injectObjectManager($objectManager, $objectManager->getClassMetadata(get_class($this)));
    }

    protected function _getIdioma($idioma = null)
    {
        if ($idioma == null) {
            if (isset($_SESSION["_sf2_attributes"]['_locale'])) {
                return $_SESSION["_sf2_attributes"]['_locale'];
            }
            return "pt_BR";
        }
        return $idioma;
    }

    protected function _getTraducao($campo, $idioma = null)
    {
        $idioma = $this->_getIdioma($idioma);

        $traducao = null;
        if ($this->$campo) {
            $traducao = $this->em->getRepository('AdminBundle:Traducao')->findOneBy(array('codigo' => $this->$campo, 'idioma' => $idioma));
        }
        if (!$traducao) {
            $texto = " ";
            $traducoesOutras = $this->em->getRepository('AdminBundle:Traducao')->findBy(array('codigo' => $this->$campo));
            if (count($traducoesOutras) == 0) {
                $texto = $this->$campo;
                $this->$campo = uniqid(true);
            } else {
                $texto = $traducoesOutras[0]->getTexto();
            }
            $traducao = new Traducao();
            $traducao->setCodigo($this->$campo);
            $traducao->setIdioma($idioma);
            $traducao->setTexto($texto ? $texto : " ");
            $this->em->persist($traducao);
            $this->em->flush();

//            /** @var CacheUtil $cacheUtil */
//            global $cacheUtil;
//            $cacheUtil->apagarCacheConteudos();
        }
        return $traducao;
    }

    protected function _setTraducaoValor($campo, $idioma = null, $texto = "")
    {
//        if ($this->_isValorTraducao($campo)) {
//            $idioma = $this->_getIdioma($idioma);
//            $traducao = $this->_getTraducao($campo, $idioma);
//            $traducao->setTexto($texto);
//            $this->em->persist($traducao);
//            $this->em->flush();
////            global $cacheUtil;
////            $cacheUtil->apagarCacheConteudos();
//
//
//        } else {

            if (method_exists($this, 'getCampoSlug') && $this->getCampoSlug() == $campo) {
                $this->slug = $this->slugfy($texto, $this);
            }
            $this->$campo = $texto;
//        }

        return $this;
    }

    public function toArray($campos = array())
    {
        $array = array();
        foreach ($campos as $campo) {
            $dato = $this->__call($campo, $array);
            if ($dato instanceof \DateTime) {

                $dato = $dato->format('Y-m-d H:m:s');
            }
            $array[] = $dato;
        }
        return $array;
    }

    protected function _getTraducaoValor($campo, $idioma = null)
    {
//        if ($this->_isValorTraducao($campo)) {
//            global $cacheUtil;
//            $traducao = $cacheUtil->_getTraducao($this->$campo, $idioma);
//            return $traducao;
//        }
        return $this->$campo;
    }

    public function __set($campo, $arguments)
    {
        return $this->__call("set" . ucfirst($campo), array($arguments));
    }

    public function __get($campo)
    {
        return $this->__call($campo, array());
    }

    public function __call($campo, $arguments)
    {

        $metodosPermitidos = array(
            "is" => "",
            "get" => "_getTraducaoValor",
            "set" => "_setTraducaoValor",
            "add" => "",
            "remove" => ""
        );
        $metodo = "get";

        foreach ($metodosPermitidos as $m => $f) {
            if (strpos($campo, $m) === 0) {
                $metodo = $m;
                $campo = lcfirst(substr($campo, strlen($m)));
                break;
            }
        }


        if (property_exists($this, $campo)) {
            $arguments = array_merge(
                array($campo, null),
                $arguments
            );

            $metodoAux = $metodosPermitidos[$metodo];
            return call_user_func_array(array($this, $metodoAux), $arguments);
        }

        $campoCamelCase = $this->toCamelCase($campo);

        foreach ($metodosPermitidos as $m => $f) {
            if (strpos($campoCamelCase, $m) === 0) {
                $metodo = $m;
                $campoCamelCase = lcfirst(substr($campoCamelCase, strlen($m)));
                break;
            }
        }


        if (property_exists($this, $campoCamelCase)) {
            $arguments = array_merge(
                array($campoCamelCase, null),
                $arguments
            );

            $metodoAux = $metodosPermitidos[$metodo];
            return call_user_func_array(array($this, $metodoAux), $arguments);
        }


        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $campo .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);

        return null;
    }

    public function toCamelCase($value)
    {
        $value = ucwords(str_replace(array('-', '_'), ' ', $value));
        $value = str_replace(' ', '', $value);
        return lcfirst($value);
    }

    protected function _isValorTraducao($campo)
    {
        $campoTraducao = $this->getCamposTraducoes();
        if (in_array($campo, $campoTraducao))
            return true;
        return false;
    }

    public function getCamposTraducoes()
    {
        return array();
    }

    public function getValorCampoImagem($imagem = '')
    {
        if ($imagem) {
            return $this->__call($imagem, array());
        }
        if (property_exists($this, "imagem")) {
            return $this->getImagem();
        }
        if (property_exists($this, "arquivo")) {
            return $this->getArquivo();
        }
        return "Campo imagem não encontrado";
    }

    public function setValorCampoImagem($imagemValor, $imagem = '')
    {
        if ($imagem && property_exists($this, $imagem)) {
            $this->__call("set" . ucfirst($imagem), array($imagemValor));
            return $this;
        }
        if (property_exists($this, "imagem")) {
            $this->setImagem($imagemValor);
            return $this;
        }
        if (property_exists($this, "arquivo")) {
            $this->setArquivo($imagemValor);
            return $this;
        }

        return "Campo imagem não encontrado";
    }

    public function getValorCampoOrigemImagem($imagem = '')
    {
        if ($imagem && property_exists($this, $imagem . "Nome")) {
            $this->__call($imagem . "Nome", array());
            return $this;
        }
        if (property_exists($this, "imagemNome")) {
            return $this->getImagemNome();
        }
        if (property_exists($this, "arquivoNome")) {
            return $this->getArquivoNome();
        }
        return "Campo imagem não encontrado";
    }

    public function setValorCampoOrigemImagem($imagemNomeValor, $imagem = '')
    {

        if ($imagem && property_exists($this, $imagem . "Nome")) {
            $this->__call("set" . ucfirst($imagem . "Nome"), array($imagemNomeValor));
            return $this;
        }
        if (property_exists($this, "imagemNome")) {
            $this->setImagemNome($imagemNomeValor);
            return $this;
        }
        if (property_exists($this, "arquivoNome")) {
            $this->setArquivoNome($imagemNomeValor);
            return $this;
        }
        return "Campo imagem não encontrado";
    }


    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     */
    public function getWebPath($imagem = '')
    {
        return "/" . $this->SERVER_PATH_TO_IMAGE_FOLDER . "/" . $this->getValorCampoImagem($imagem);
    }


    /**
     * @param int $width
     * @param int $height
     * @param string $cropratio
     * @return string
     * @internal param string $strArgs Uma strinf contendo os parametros necessarios para gerar uma thumb pelo arquivo resize. ex: width=100&height=150&cropratio=0.66:1
     */
    public function getWebThumb($width = 100, $height = 100, $cropratio = '1:1', $imagem = '')
    {

        return resizeImage($this->SERVER_PATH_TO_IMAGE_FOLDER . '/', $this->getValorCampoImagem($imagem), $width, $height, $cropratio);;
    }


    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return String
     */
    public function getFullWebPath(\Symfony\Component\HttpFoundation\Request $request)
    {
        return $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . $this->getWebPath();
    }


    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return String
     */
    public function getFullWebThumb(\Symfony\Component\HttpFoundation\Request $request, $width = 100, $height = 100, $cropratio = '1:1')
    {
        return $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . $this->getWebThumb();
    }

    /**
     * Remove the file on server
     */
    public function removeFile($imagem = '')
    {
        if ($this->getValorCampoImagem($imagem) && $this->getValorCampoImagem($imagem) != "padrao.jpg" && file_exists(substr($this->getWebPath($imagem), 1))) {
            unlink(substr($this->getWebPath($imagem), 1));
            return true;
        }
        return false;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     */
    public function upload($imagem = '')
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues

        $originalNome = $this->getFile()->getClientOriginalName();
        $uniqName = uniqid(true) . strstr($originalNome, '.');

        // move takes the target directory and target filename as params
        $this->getFile()->move(
            $this->SERVER_PATH_TO_IMAGE_FOLDER,
            $uniqName
        );

        $this->removeFile($imagem);


        // set the path property to the filename where you've saved the file
        $this->setValorCampoImagem($uniqName, $imagem);
        $this->setValorCampoOrigemImagem($originalNome, $imagem);

        // clean up the file property as you won't need it anymore
        $this->setFile(null);
    }

    /**
     * Lifecycle callback to upload the file to the server
     */
    public function lifecycleFileUpload()
    {
        $this->upload();
    }

    public function slugfy($string, $objeto = null)
    {
        $characters = array(
            "Á" => "A", "Ç" => "c", "É" => "e", "Í" => "i", "Ñ" => "n", "Ó" => "o", "Ú" => "u",
            "á" => "a", "ç" => "c", "é" => "e", "í" => "i", "ñ" => "n", "ó" => "o", "ú" => "u",
            "à" => "a", "è" => "e", "ì" => "i", "ò" => "o", "ù" => "u"
        );

        $acentos = array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/");
        $string = preg_replace($acentos, explode(" ", "a A e E i I o O u U n N"), $string);

        $string = strtr($string, $characters);
        $string = strtolower(trim($string));
        $string = preg_replace("/[^a-z0-9-]/", "-", $string);
        $string = preg_replace("/-+/", "-", $string);

        if (substr($string, strlen($string) - 1, strlen($string)) === "-") {
            $string = substr($string, 0, strlen($string) - 1);
        }

        $count = 0;
        $sufixo = "";
        if ($objeto) {
            $objetoClass = get_class($objeto);
            $objetoId = $objeto->getId() ? $objeto->getId() : "";

            do {
                $sufixo = $count > 0 ? "-" . $count : "";
                $dql = "SELECT a.id FROM " . $objetoClass . " a WHERE a.slug = :slug and a.id <> :objeto_id";
                $query = $this->em->createQuery($dql)
                    ->setParameter("slug", $string . $sufixo)
                    ->setParameter("objeto_id", $objetoId)
                    ->execute();
                $count++;
            } while (count($query) > 0);

        }

        return $string . $sufixo;
    }


}
