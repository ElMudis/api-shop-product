<?php

namespace _Base\BackendBundle\Entity\Base;



use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Hateoas\Configuration\Annotation as Hateoas;


use _Base\BackendBundle\Entity\ItemCart;
use _Base\BackendBundle\Entity\Client;



/**
 *
 *
 * @ORM\MappedSuperclass
 */
class BaseCart extends BaseEntity
{
	/**
	 *
	 *
	 * @Serializer\Exclude()
	 */
	protected $SERVER_PATH_TO_IMAGE_FOLDER = "uploads/cart";

	/**
	 *
	 *
	 * @var int
	 *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
	 */
	protected $id;


	/**
	 * TotalPrice
	 *
	 * @var float
	 *
	 * @ORM\Column(type="float",  nullable = false )
	 * @Assert\NotBlank(message = "TotalPrice e obrigatorio")    */
	protected $totalPrice;
	/**
	 * One Cart have many ItemCarts.
	 *
	 * @ORM\OneToMany(targetEntity="ItemCart", mappedBy="cart")
	 */
	protected $item_cart;

	/**
	 * One Cart have one Client.
	 *
	 * @ORM\OneToOne(targetEntity="Client", mappedBy="cart")
     * @Serializer\Exclude()
	 */
	protected $client;

	public static function getCamposObrigatorios() {
		return array( 'totalPrice');
	}
	public static function getCampos() {
		return array('ordem', 'totalPrice', 'item_cart', 'client');
	}

	public static function getFrontendObjetoNome() {
		return "cart";
	}

	public function getCamposTraducoes() {
		return array(
			'totalPrice',
		);
	}

	/**
	 *
	 *
	 * @return string
	 */
	public function __toString() {
		return $this->getTotalPrice();
	}





	/**
	 * Constructor
	 */
	public function __construct() {
		$this->item_cart = new \Doctrine\Common\Collections\ArrayCollection();
	}













	/**
	 * Add item_cart
	 *
	 * @param \_Base\BackendBundle\Entity\ItemCart $item_cart
	 *
	 * @return Equipetest
	 */
	public function addItem_cart(\_Base\BackendBundle\Entity\ItemCart $item_cart) {
		$this->item_cart[] = $item_cart;
		return $this;
	}

	/**
	 * Remove item_cart
	 *
	 * @param \_Base\BackendBundle\Entity\ItemCart $item_cart
	 */
	public function removeItem_cart(\_Base\BackendBundle\Entity\ItemCart $item_cart) {
		$this->item_cart->removeElement($item_cart);
	}

	/**
	 * Get item_cart
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getItem_cart() {
		return $this->item_cart;
	}


	public static function getListaItem_cart(\Symfony\Component\DependencyInjection\Container $container) {
		$em = $container->get('doctrine')->getManager();

		$dql = "SELECT a.id as id, a1.texto as titulo FROM BackendBundle:ItemCart a LEFT JOIN BackendBundle:Traducao a1 WITH a.title = a1.codigo";
		$equiposTestes = $em->createQuery($dql)->execute();

		$listaEquipeTest = array();
		foreach ($equiposTestes as $equipo) {
			$listaEquipeTest[$equipo['id']] = $equipo['titulo'];
		}

		return $listaEquipeTest;

	}


	/**
	 * Set client
	 *
	 * @param \_Base\BackendBundle\Entity\Client $client
	 *
	 * @return Cart
	 */
	public function setClient($client)
	{
		if(!is_object($client) && !is_null($client) ){
			$em = $this->em;
			$client = $em->getRepository('BackendBundle:Client')->find($client);
		}
		$this->client = $client;
		return $this;
	}

	/**
	 * Get client
	 *
	 * @return \_Base\BackendBundle\Entity\Client
	 */
	public function getClient()
	{
		return $this->client;
	}



	public static function getListaClient(\Symfony\Component\DependencyInjection\Container $container)
	{
		$em = $container->get('doctrine')->getManager();

		$dql = "SELECT a.id as id, a1.texto as titulo FROM BackendBundle:Client a LEFT JOIN BackendBundle:Traducao a1 WITH a.name = a1.codigo";
		$equiposTestes = $em->createQuery($dql)->execute();

		$listaEquipeTest = array();
		foreach ($equiposTestes as $equipo){
			$listaEquipeTest[$equipo['id']] = $equipo['titulo'];
		}

		return $listaEquipeTest;

	}





}
