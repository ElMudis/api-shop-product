<?php

namespace _Base\BackendBundle\Entity\Base;



use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Hateoas\Configuration\Annotation as Hateoas;


use _Base\BackendBundle\Entity\ItemCart;



/**
 *
 *
 * @ORM\MappedSuperclass
 */
class BaseProduct extends BaseEntity
{
	/**
	 *
	 *
	 * @Serializer\Exclude()
	 */
	protected $SERVER_PATH_TO_IMAGE_FOLDER = "uploads/product";

	/**
	 *
	 *
	 * @var int
	 *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
	 */
	protected $id;


	/**
	 * Title
	 *
	 * @var string
	 *
	 * @ORM\Column(type="string",  nullable = false )
	 * @Assert\NotBlank(message = "Title e obrigatorio")    */
	protected $title;
	/**
	 * Price
	 *
	 * @var int
	 *
	 * @ORM\Column(type="float",  nullable = false )
	 * @Assert\NotBlank(message = "Price e obrigatorio")*/
	protected $price;

	/**
	 * One Product have many ItemCarts.
	 *
	 * @ORM\OneToMany(targetEntity="ItemCart", mappedBy="product")
     * @Serializer\Exclude()
	 */
	protected $item_cart;

	/**
	 * Slug
	 *
	 * @var string
	 *
	 * @ORM\Column(type="string",  nullable = true )
	 * */
	protected $slug;
	public static function getCamposObrigatorios() {
		return array( 'title', 'price');
	}
	public static function getCampos() {
		return array('title', 'price', 'item_cart', 'slug');
	}

	public static function getFrontendObjetoNome() {
		return "product";
	}

	public function getCamposTraducoes() {
		return array(
			'title',
			'price',
		);
	}

	/**
	 *
	 *
	 * @return string
	 */
	public function __toString() {
		return $this->getTitle();
	}





	/**
	 * Constructor
	 */
	public function __construct() {
		$this->item_cart = new \Doctrine\Common\Collections\ArrayCollection();
	}



	public static function getCampoSlug() {
		return "title";
	}




	/**
	 * Add item_cart
	 *
	 * @param \_Base\BackendBundle\Entity\ItemCart $item_cart
	 *
	 * @return Equipetest
	 */
	public function addItem_cart(\_Base\BackendBundle\Entity\ItemCart $item_cart) {
		$this->item_cart[] = $item_cart;
		return $this;
	}

	/**
	 * Remove item_cart
	 *
	 * @param \_Base\BackendBundle\Entity\ItemCart $item_cart
	 */
	public function removeItem_cart(\_Base\BackendBundle\Entity\ItemCart $item_cart) {
		$this->item_cart->removeElement($item_cart);
	}

	/**
	 * Get item_cart
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getItem_cart() {
		return $this->item_cart;
	}


	public static function getListaItem_cart(\Symfony\Component\DependencyInjection\Container $container) {
		$em = $container->get('doctrine')->getManager();

		$dql = "SELECT a.id as id, a1.texto as titulo FROM BackendBundle:ItemCart a LEFT JOIN BackendBundle:Traducao a1 WITH a.title = a1.codigo";
		$equiposTestes = $em->createQuery($dql)->execute();

		$listaEquipeTest = array();
		foreach ($equiposTestes as $equipo) {
			$listaEquipeTest[$equipo['id']] = $equipo['titulo'];
		}

		return $listaEquipeTest;

	}







}
