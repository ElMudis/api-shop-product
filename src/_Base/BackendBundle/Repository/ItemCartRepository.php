<?php

namespace _Base\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ItemCartRepository extends EntityRepository
{
	use ParamFetcherRepositoryTrait;

	protected function getFilters( $alias, $queryBuilder) {
		return [

		'title' => function($value) use ($alias, $queryBuilder) {
			return $queryBuilder
			->andWhere($alias . '.title LIKE :title')
			->setParameter(':title', sprintf('%%%s%%', $value));
		},
		'price' => function($value) use ($alias, $queryBuilder) {
			return $queryBuilder
			->andWhere($alias . '.price LIKE :price')
			->setParameter(':price', sprintf('%s', $value));
		},



		'quantity' => function($value) use ($alias, $queryBuilder) {
			return $queryBuilder
			->andWhere($alias . '.quantity LIKE :quantity')
			->setParameter(':quantity', sprintf('%s', $value));
		},



		];
	}
}
