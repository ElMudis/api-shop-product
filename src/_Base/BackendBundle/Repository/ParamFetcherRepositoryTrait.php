<?php

namespace _Base\BackendBundle\Repository;

use FOS\RestBundle\Request\ParamFetcherInterface;

trait ParamFetcherRepositoryTrait
{
	protected $_alias;
	
	abstract protected function getFilters($alias, $queryBuilder);
	
	public function findByParamFetcher(ParamFetcherInterface $paramFetcher, $alias)
	{
		$queryBuilder = $this
			->createQueryBuilder($alias)
		;
		
		$filtersFunctions = $this->getFilters($alias, $queryBuilder);

		
		foreach ($paramFetcher->all() as $key => $value)
		{
			if ($value === "" || $value === null || !isset($filtersFunctions[$key])) {
				continue;
			}

			$queryBuilder = $filtersFunctions[$key]($value);
		}

		
		return $queryBuilder;
	}
	
	public function createQueryBuilder($alias, $indexBy = null)
	{
		$this->_alias = $alias;
		return parent::createQueryBuilder($alias, $indexBy);
	}
	
	public function getAlias()
	{
		return $this->_alias;
	}
	
}