<?php





namespace _Base\BackendBundle\Controller\Entidades;

use _Base\BackendBundle\Entity\Product;
use _Base\BackendBundle\Form\ProductType;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use Hateoas\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Response;
use _Base\ApiBundle\Controller\ApiController;


class ProductController extends ApiController
{

    protected static function getDefaultLimit()
    {
        return 3;
    }

	/**
	 * Products List
	 *
	 * @ApiDoc(resource=true)
	 *
	 * @QueryParam(name="page", description="Number of page. Default 1")
	 * @QueryParam(name="limit", description="Number of itens by page. Default 3")
	 * @QueryParam(name="title", description="Filter by title")
	 *
	 * @param ParamFetcherInterface $paramFetcher
	 *
	 * @return Response
	 */
	public function cgetAction( ParamFetcherInterface $paramFetcher) {
		$queryBuilder = $this
		->getRepository(Product::class)
		->findByParamFetcher($paramFetcher, 'product');

		return $this->createResponseForCollection($paramFetcher, $queryBuilder, new Route('get_products'));
	}

	/**
	 * Get a Product
	 *
	 * @ApiDoc
	 *
	 * @param integer $id ID do registro
	 * @return Response
	 */
	public function getAction($id) {
		return $this->createResponseForFindOne(Product::class, $id);
	}

	/**
	 * Add new Product
	 *
	 * @ApiDoc(input={"class"="_Base\BackendBundle\Form\ProductType", "name"=""})
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function postAction(Request $request) {
		return $this->registration($request);
	}

	/**
	 * Update a Product
	 *
	 * @ApiDoc(input={"class"="_Base\BackendBundle\Form\ProductType", "name"=""})
	 *
	 * @param Request $request
	 * @param integer $id      ID da categoria
	 *
	 * @return Response
	 */
	public function putAction(Request $request, $id) {
		return $this->registration($request, $id);
	}

	/**
	 * Remove a Product
	 *
	 * @ApiDoc()
	 *
	 * @param integer $id ID da categoria
	 *
	 * @return Response
	 */
	public function deleteAction($id) {
		$repository = $this->getRepository(Product::class);
		return $this->createReponseForDeleteOne($repository, $id);
	}

	/**
	 *
	 * @param Request $request
	 * @param null    $id
	 *
	 * @return Response
	 */
	private function registration(Request $request, $id = null) {

		$isPost = $request->isMethod(Request::METHOD_POST);

		$sentData = json_decode($request->getContent(), true);
		$em = $this->getDoctrine()->getManager();

		$object = $isPost
			? new Product()
			: $em->getRepository(Product::class)->find($id);


		if (!$object) {
			return $this->handleView($this->view(null, Codes::HTTP_NOT_FOUND));
		}

		$form = $this->createForm(ProductType::class, $object, ($isPost ? [] : ['method' => Request::METHOD_PUT]));
		$form->submit($sentData, $isPost);

		if (!$form->isValid()) {
			return $this->handleView($this->view($form->getErrors(), Codes::HTTP_BAD_REQUEST));
		}

		$em->persist($object);
		$em->flush();

		return $this->handleView($this->view($object, Codes::HTTP_CREATED));
	}

}
