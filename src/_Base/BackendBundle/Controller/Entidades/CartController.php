<?php


namespace _Base\BackendBundle\Controller\Entidades;

use _Base\BackendBundle\Entity\Cart;
use _Base\BackendBundle\Entity\ItemCart;
use _Base\BackendBundle\Form\CartType;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use Hateoas\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Response;
use _Base\ApiBundle\Controller\ApiController;


class CartController extends ApiController
{

    /**
     * Get a cart
     *
     * @ApiDoc(resource=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $cart = $em->find('BackendBundle:Cart', $id);
        if (!$cart) {
            return $this->handleView($this->view(null, Codes::HTTP_NOT_FOUND));
        }
        return $this->handleView($this->view($cart, Codes::HTTP_OK));
    }

    /**
     * Remove cart
     *
     * @ApiDoc()
     *
     * @param integer $id ID da categoria
     *
     * @return Response
     */
    public function deleteAction($id)
    {
        $repository = $this->getRepository(Cart::class);
        return $this->createReponseForDeleteOne($repository, $id);
    }

    /**
     * Add a product to the cart
     *
     * @ApiDoc(resource=true, input={"class"="_Base\BackendBundle\Form\CartType", "name"=""})
     *
     * @param Request $request
     * @param integer $cart_id Cart id*
     *
     * @return Response
     */
    public function postProdutosAction(Request $request, $cart_id)
    {
        $em = $this->getDoctrine()->getManager();
        $cart = $em->find('BackendBundle:Cart', $cart_id);

        if (!$cart) {
            return $this->handleView($this->view(null, Codes::HTTP_NOT_FOUND));
        }

        $sentData = json_decode($request->getContent(), true);

        $product = $em->find('BackendBundle:Product', $sentData["product"]);


        $cartItem = $em->getRepository("BackendBundle:ItemCart")->findOneBy(array("product" => $product, "cart" => $cart));
        $allCartItem = $em->getRepository("BackendBundle:ItemCart")->findBy(array("cart" => $cart));

        if (!$cartItem) {
            if (count($allCartItem) < 3) {
                $cartItem = new ItemCart();

                $cartItem->setManager($em);
                $cartItem->setProduct($product);
                $cartItem->setCart($cart);
                $cartItem->setTitle($product->getTitle());
                $cartItem->setPrice($product->getPrice());
                $cartItem->setQuantity($sentData["quantity"]);

                $em->persist($cart);
                $em->persist($product);
            }else{
                return $this->handleView($this->view("Many product in the cart", Codes::HTTP_OK));
            }
        } else {
            $cartItem->setQuantity($cartItem->getQuantity() + $sentData["quantity"]);
        }

        $em->persist($cartItem);
        $em->flush();


        return $this->handleView($this->view($cart, Codes::HTTP_OK));
    }

    /**
     * Remove product from the cart
     *
     * @ApiDoc(input={"class"="_Base\BackendBundle\Form\CartType", "name"=""})
     *
     * @param Request $request
     * @param integer $cart_id Cart id
     * @param integer $id Cart id
     *
     * @return Response
     */
    public function deleteProdutosAction(Request $request, $cart_id)
    {
        $em = $this->getDoctrine()->getManager();
        $cart = $em->find('BackendBundle:Cart', $cart_id);

        if (!$cart) {
            return $this->handleView($this->view(null, Codes::HTTP_NOT_FOUND));
        }

        $sentData = json_decode($request->getContent(), true);

        $product = $em->find('BackendBundle:Product', $sentData["product"]);


        $cartItem = $em->getRepository("BackendBundle:ItemCart")->findOneBy(array("product" => $product, "cart" => $cart));

        if ($cartItem) {
            if ($cartItem->getQuantity() > $sentData["quantity"]) {
                $cartItem->setQuantity($cartItem->getQuantity() - $sentData["quantity"]);
                $em->persist($cartItem);
            } else {
                $em->remove($cartItem);
            }
        }


        $em->flush();


        return $this->handleView($this->view($cart, Codes::HTTP_OK));
    }

    /**
     * Cadastra ou atualiza o registro conforme a definição do METHOD.
     *
     * @param Request $request
     * @param null $id
     *
     * @return Response
     */
    private function registration(Request $request, $id = null)
    {
        $isPost = $request->isMethod(Request::METHOD_POST);

        $sentData = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();

        $object = $isPost
            ? new Cart()
            : $em->getRepository(Cart::class)->find($id);

        if (!$object) {
            return $this->handleView($this->view(null, Codes::HTTP_NOT_FOUND));
        }

        $form = $this->createForm(CartType::class, $object, ($isPost ? [] : ['method' => Request::METHOD_PUT]));
        $form->submit($sentData, $isPost);

        if (!$form->isValid()) {
            return $this->handleView($this->view($form->getErrors(), Codes::HTTP_BAD_REQUEST));
        }

        $em->persist($object);
        $em->flush();

        return $this->handleView($this->view($object, Codes::HTTP_CREATED));
    }

}
